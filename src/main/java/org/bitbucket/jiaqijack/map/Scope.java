/*
 * Copyright 刘珈奇
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jiaqijack.map;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * A scope represents a tree of delegating maps.
 * <p>
 * A Scope may have child scopes which it encloses. It may have a parent scope which encloses it. Values defined in a
 * parent scope will be available via {@link java.util.Map#get(Object)} on itself, unless the scope itself defines (and
 * therefore overshadows) that entry.
 * <p>
 * Changes to a scope will be available to it's child scopes unless they overshadow the corresponding key.
 * <p>
 * The remove and clear operations are not supported on Scopes because parent scopes cannot be directly accessed or
 * altered.
 *
 * @param <S>  The address element type for looking uo child scopes
 * @param <K>  The key type for the map
 * @param <V>  The value type for the map
 * @param <T>  The implementation type (used to type returns from {@link #getScope(List)} in sub-classes)
 */
public interface Scope<S, K, V, T extends Scope<S, K, V, T>> extends Map<K, V> {
    /**
     * Resolves a child scope.
     * <p>
     * The child scope should delegate to its parent scopes for value lookups.
     * <p>
     * Unless otherwise noted, implementations should guarantee lazy initialization of child scopes.
     *
     * @param scopeKeys  The subtree address expressed as a sequence of scopeKeys
     *
     * @return A scope which is a subtree of this scope.
     */
    T getScope(List<S> scopeKeys);

    /**
     * Puts a key value pair into an immediate child scope.
     *
     * @param scopeKey  The name of the child scope
     * @param key  The key for the value being stored
     * @param value  The value being stored
     *
     * @return  The value displaced or shadowed. See
     * {@link org.bitbucket.jiaqijack.map.DelegatingMap#put(Object, Object)}
     */
    default V putScope(final S scopeKey, final K key, final V value) {
        return getScope(Collections.singletonList(scopeKey)).put(key, value);
    }
}
