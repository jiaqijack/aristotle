/*
 * Copyright 刘珈奇
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jiaqijack;

import static org.bitbucket.jiaqijack.jaxrs.config.ErrorMessageFormat.TWO_VALUES_OF_THE_SAME_KEY;

import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Utils for dealing with streams.
 */
public class StreamUtils {
    /**
     * Returns a merger function, suitable for use in
     * {@link java.util.Map#merge(Object, Object, java.util.function.BiFunction)} or
     * {@link java.util.stream.Collectors#toMap(Function, Function, BinaryOperator)}, which always throws
     * {@link java.lang.IllegalStateException}. This can be used to enforce the assumption that the elements being
     * collected are distinct.
     *
     * @param <T>  The type of input arguments to the merge function
     *
     * @return a merge function which always throw {@link java.lang.IllegalStateException}
     *
     * @see Collectors#throwingMerger()
     */
    public static <T> BinaryOperator<T> throwingMerger() {
        return (u, v) -> {
            throw new IllegalStateException(TWO_VALUES_OF_THE_SAME_KEY.format(u, v));
        };
    }

    /**
     * Returns a {@link java.util.stream.Collector} that creates a {@link java.util.LinkedHashMap} using the given
     * {@link java.util.function.Function key and value functions}. This {@link java.util.stream.Collector} assumes the
     * elements being collected are distinct.
     *
     * @param keyMapper  {@link java.util.function.Function Mapping function} for the key
     * @param valueMapper  {@link java.util.function.Function Mapping function} for the value
     * @param <S>  Type of the objects being collected
     * @param <K>  Type of the keys
     * @param <V>  Type of the values
     *
     * @return a {@link java.util.stream.Collector} that creates a {@link java.util.LinkedHashMap} using the given
     * {@link java.util.function.Function key and value functions}.
     *
     * @throws IllegalStateException if multiple values are associated with the same key
     *
     * @see Collectors#toMap(Function, Function, BinaryOperator, Supplier)
     */
    public static <S, K, V> Collector<S, ?, LinkedHashMap<K, V>> toLinkedMap(
            final Function<? super S, ? extends K> keyMapper,
            final Function<? super S, ? extends V> valueMapper
    ) {
        return Collectors.toMap(keyMapper, valueMapper, throwingMerger(), LinkedHashMap::new);
    }

    /**
     * Returns a {@link java.util.stream.Collector} that creates a {@link java.util.Map} using the given
     * {@link java.util.function.Function key and value functions}. This {@link java.util.stream.Collector} assumes the
     * elements being collected are distinct.
     * <p>
     * The concrete type of the {@link java.util.Map} being created is given by a {@link java.util.function.Supplier}.
     *
     * @param keyMapper  {@link java.util.function.Function Mapping function} for the key
     * @param valueMapper  {@link java.util.function.Function Mapping function} for the value
     * @param mapSupplier  A {@link java.util.function.Supplier} which provides a new, empty {@link java.util.Map} into
     * which the results will be inserted
     * @param <S>  Type of the objects being collected
     * @param <K>  Type of the keys
     * @param <V>  Type of the values
     * @param <M>  Type of {@link java.util.Map} being collected into
     *
     * @return a {@link java.util.stream.Collector} that creates a {@link java.util.Map} using the given
     * {@link java.util.function.Function key and value functions}.
     *
     * @throws IllegalStateException if multiple values are associated with the same key
     *
     * @see Collectors#toMap(Function, Function, BinaryOperator, Supplier)
     */
    public static <S, K, V, M extends Map<K, V>> Collector<S, ?, M> toMap(
            final Function<? super S, ? extends K> keyMapper,
            final Function<? super S, ? extends V> valueMapper,
            final Supplier<M> mapSupplier
    ) {
        return Collectors.toMap(keyMapper, valueMapper, throwingMerger(), mapSupplier);
    }

    /**
     * Returns a {@link java.util.stream.Collector} that creates a {@link java.util.LinkedHashMap} using the given
     * {@link java.util.function.Function key function}. This {@link java.util.stream.Collector} assumes the elements
     * being collected are distinct.
     *
     * @param keyMapper  {@link java.util.function.Function Mapping function} for the key
     * @param <S>  Type of the objects being collected and the values of and the values of the dictionary / map
     * @param <K>  Type of the keys
     *
     * @return a {@link java.util.stream.Collector} that creates a {@link java.util.LinkedHashMap} using the given
     * {@link java.util.function.Function key function}
     *
     * @throws IllegalStateException if multiple values are associated with the same key
     *
     * @see Collectors#toMap(Function, Function, BinaryOperator, Supplier)
     */
    public static <S, K> Collector<S, ?, LinkedHashMap<K, S>> toLinkedDictionary(
            final Function<? super S, ? extends K> keyMapper
    ) {
        return Collectors.toMap(keyMapper, Function.identity(), throwingMerger(), LinkedHashMap::new);
    }

    /**
     * Returns a {@link java.util.stream.Collector} that creates a dictionary using the given
     * {@link java.util.function.Function key function} and the given {@link java.util.function.Supplier map supplier}.
     * This {@link java.util.stream.Collector} assumes the elements being collected are distinct.
     *
     * @param keyMapper  {@link java.util.function.Function Mapping function} for the key
     * @param mapSupplier  A {@link java.util.function.Supplier} which provides a new, empty {@link java.util.Map} into
     * which the results will be inserted
     * @param <S>  Type of the objects being collected and the values of the dictionary / map
     * @param <K>  Type of the keys
     * @param <M>  Type of {@link java.util.Map} being collected into
     *
     * @return a {@link java.util.stream.Collector} that creates a dictionary using the given
     * {@link java.util.function.Function key function} and the given {@link java.util.function.Supplier map supplier}
     *
     * @throws IllegalStateException if multiple values are associated with the same key
     *
     * @see Collectors#toMap(Function, Function, BinaryOperator, Supplier)
     */
    public static <S, K, M extends Map<K, S>> Collector<S, ?, M> toDictionary(
            final Function<? super S, ? extends K> keyMapper,
            final Supplier<M> mapSupplier
    ) {
        return Collectors.toMap(keyMapper, Function.identity(), throwingMerger(), mapSupplier);
    }

    /**
     * Collects a {@link java.util.stream.Stream} into an unmodifiable set preserving first-in ordering.
     *
     * @param values  The stream of values to be collected
     * @param <T>  The type of the value stream
     *
     * @return  an unmodifiable copy of a {@link java.util.LinkedHashSet} view of the stream
     */
    public static <T> Set<T> toUnmodifiableSet(final Stream<T> values) {
        return Collections.unmodifiableSet((Set<T>) values.collect(Collectors.toCollection(LinkedHashSet::new)));
    }

    /**
     * Negate the given {@link java.util.function.Predicate}.
     *
     * @param predicate  {@link java.util.function.Predicate} to negate
     * @param <T>  Type of the predicate being negated
     *
     * @return the negated {@link java.util.function.Predicate}
     */
    public static <T> Predicate<T> not(final Predicate<T> predicate) {
        return predicate.negate();
    }

    /**
     * Returns a {@link java.util.function.Function} that can be used to do unchecked casts without generating a
     * compiler warning.
     * <p>
     * Essentially the same as annotating the case with {@code @SuppressWarnings("unchecked")}, which you cannot do
     * inside a {@link java.util.stream.Stream}.
     *
     * @param targetType  Class to case the operand (function parameter) to. Used only to set the generic type
     * @param <T>  Operand type (type of function parameter)
     * @param <R>  Cast type
     *
     * @return the {@link java.util.function.Function} that will do the cast
     */
    public static <T, R> Function<T, R> uncheckedCast(final Class<R> targetType) {
        return (T operand) -> {
            @SuppressWarnings("unchecked")
            final R castedOperand = (R) operand;
            return castedOperand;
        };
    }

    /**
     * Copies a {@link java.util.Set}, adds a value to it and return the new set.
     *
     * @param set  The original {@link java.util.Set} being copied
     * @param value  The value being appended to the {@link java.util.Set}
     * @param <T>  The type of the {@link java.util.Set}
     *
     * @return the new {@link java.util.Set} containing the additional value
     */
    public static <T> Set<T> append(final Set<T> set, final T value) {
        final HashSet<T> result = new HashSet<>(set);
        result.add(value);
        return result;
    }

    /**
     * Merges two {@link java.util.Set sets} without modifying either.
     * <p>
     * This method implements {@link java.util.function.BinaryOperator} for use in {@link java.util.stream.Stream}
     * reductions.
     *
     * @param first  One input {@link java.util.Set}
     * @param second  The other input {@link java.util.Set}
     * @param <T>  The type of the underlying {@link java.util.Set sets}
     *
     * @return a new {@link java.util.Set} containing the values of the original {@link java.util.Set sets}
     */
    public static <T> Set<T> setMerge(final Set<T> first, final Set<T> second) {
        return Stream.concat(first.stream(), second.stream())
                .collect(Collectors.toSet());
    }
}
