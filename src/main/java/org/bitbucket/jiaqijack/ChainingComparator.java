/*
 * Copyright 刘珈奇
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jiaqijack;

import java.util.Comparator;
import java.util.List;

/**
 * A comparator which accepts a list of other {@link java.util.Comparator comparators} to apply in order until an
 * imbalance is found.
 *
 * @param <T>  Type of objects that may be compared by this comparator
 */
public class ChainingComparator<T> implements Comparator<T> {
    /**
     * A list of {@link java.util.Comparator comparators} that will be applied in chain.
     */
    private final List<Comparator<T>> comparators;

    /**
     * Constructor.
     *
     * @param comparators  A list of {@link java.util.Comparator comparators} that will be applied in chain
     */
    public ChainingComparator(final List<Comparator<T>> comparators) {
        this.comparators = comparators;
    }

    @Override
    public int compare(final T first, final T second) {
        return comparators.stream()
                .mapToInt(comparator -> comparator.compare(first, second))
                .filter(result -> result != 0)
                .findFirst()
                .orElse(0);
    }
}
