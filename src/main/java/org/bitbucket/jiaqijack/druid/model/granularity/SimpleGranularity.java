/*
 * Copyright 刘珈奇
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jiaqijack.druid.model.granularity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.bitbucket.jiaqijack.druid.model.query.TimeSeriesQuery;

import java.util.Locale;

/**
 * Simple granularities are specified as a string and bucket timestamps by their UTC time, such as days starting at
 * 00:00 UTC.
 * <p>
 * For example, suppose you have data below stored in Druid with millisecond ingestion granularity
 * <pre>
 *     {"timestamp": "2013-08-31T01:02:33Z", "page": "AAA", "language" : "en"}
 *     {"timestamp": "2013-09-01T01:02:33Z", "page": "BBB", "language" : "en"}
 *     {"timestamp": "2013-09-02T23:32:45Z", "page": "CCC", "language" : "en"}
 *     {"timestamp": "2013-09-03T03:32:45Z", "page": "DDD", "language" : "en"}
 * </pre>
 * After submitting a groupBy query with {@code hour} granularity,
 * <pre>
 * {
 *     "queryType":"groupBy",
 *     "dataSource":"my_dataSource",
 *     "granularity":"hour",
 *     "dimensions":[
 *         "language"
 *     ],
 *     "aggregations":[ {
 *         "type":"count",
 *         "name":"count"
 *     } ],
 *     "intervals":[
 *         "2000-01-01T00:00Z/3000-01-01T00:00Z"
 *     ]
 * }
 * </pre>
 * you will get
 * <pre>
 * [
 *     {
 *         "version" : "v1",
 *         "timestamp" : "2013-08-31T01:00:00.000Z",
 *         "event" : {
 *             "count" : 1,
 *             "language" : "en"
 *     }
 *     }, {
 *         "version" : "v1",
 *         "timestamp" : "2013-09-01T01:00:00.000Z",
 *         "event" : {
 *             "count" : 1,
 *             "language" : "en"
 *         }
 *     }, {
 *         "version" : "v1",
 *         "timestamp" : "2013-09-02T23:00:00.000Z",
 *         "event" : {
 *             "count" : 1,
 *             "language" : "en"
 *         }
 *     }, {
 *         "version" : "v1",
 *         "timestamp" : "2013-09-03T03:00:00.000Z",
 *         "event" : {
 *             "count" : 1,
 *             "language" : "en"
 *         }
 *     }
 * ]
 * </pre>
 * Note that all the empty buckets are discarded.
 * <p>
 * If you change the granularity to {@code day}, you will get
 * <pre>
 * [
 *     {
 *         "version" : "v1",
 *         "timestamp" : "2013-08-31T00:00:00.000Z",
 *         "event" : {
 *             "count" : 1,
 *             "language" : "en"
 *     }
 *     }, {
 *         "version" : "v1",
 *         "timestamp" : "2013-09-01T00:00:00.000Z",
 *         "event" : {
 *             "count" : 1,
 *             "language" : "en"
 *         }
 *     }, {
 *         "version" : "v1",
 *         "timestamp" : "2013-09-02T00:00:00.000Z",
 *         "event" : {
 *             "count" : 1,
 *             "language" : "en"
 *         }
 *     }, {
 *         "version" : "v1",
 *         "timestamp" : "2013-09-03T00:00:00.000Z",
 *         "event" : {
 *             "count" : 1,
 *             "language" : "en"
 *         }
 *     }
 * ]
 * </pre>
 * If you change the granularity to {@code none}, you will get the same results as setting it to the ingestion
 * granularity.
 * <pre>
 * [
 *     {
 *         "version" : "v1",
 *         "timestamp" : "2013-08-31T01:02:33.000Z",
 *         "event" : {
 *             "count" : 1,
 *             "language" : "en"
 *         }
 *     }, {
 *         "version" : "v1",
 *         "timestamp" : "2013-09-01T01:02:33.000Z",
 *         "event" : {
 *             "count" : 1,
 *             "language" : "en"
 *         }
 *     }, {
 *         "version" : "v1",
 *         "timestamp" : "2013-09-02T23:32:45.000Z",
 *         "event" : {
 *              "count" : 1,
 *              "language" : "en"
 *          }
 *      }, {
 *          "version" : "v1",
 *          "timestamp" : "2013-09-03T03:32:45.000Z",
 *          "event" : {
 *              "count" : 1,
 *              "language" : "en"
 *          }
 *      }
 * ]
 * </pre>
 * Having a query granularity smaller than the ingestion granularity does not make sense, because information about that
 * smaller granularity is not present in the indexed data. If the query granularity is smaller than the ingestion
 * granularity, Druid produces results that are equivalent to having set the query granularity to the ingestion
 * granularity.
 * <p>
 * If you change the granularity to {@code all}, you will get everything aggregated in 1 bucket:
 * <pre>
 * [
 *     {
 *         "version" : "v1",
 *         "timestamp" : "2000-01-01T00:00:00.000Z",
 *         "event" : {
 *             "count" : 4,
 *             "language" : "en"
 *         }
 *     }
 * ]
 * </pre>
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum SimpleGranularity implements Granularity {

    /**
     * {@code all} buckets everything into a single bucket.
     */
    ALL,

    /**
     * {@code none} does not bucket data (it actually uses the granularity of the index - minimum here is {@code none})
     * which means millisecond granularity). Using {@code none} in a
     * {@link TimeSeriesQuery} is currently not recommended, because the system will try to generate 0 values for all
     * milliseconds that did not exist, which is often a lot.
     */
    NONE,

    @SuppressWarnings({"JavadocVariable"})
    SECOND,

    @SuppressWarnings({"JavadocVariable"})
    MINUTE,

    @SuppressWarnings({"JavadocVariable"})
    FIFTEEN_MINUTE,

    @SuppressWarnings({"JavadocVariable"})
    THIRTY_MINUTE,

    @SuppressWarnings({"JavadocVariable"})
    HOUR,

    @SuppressWarnings({"JavadocVariable"})
    DAY,

    @SuppressWarnings({"JavadocVariable"})
    WEEK,

    @SuppressWarnings({"JavadocVariable"})
    MONTH,

    @SuppressWarnings({"JavadocVariable"})
    QUARTER,

    @SuppressWarnings({"JavadocVariable"})
    YEAR;

    @JsonProperty(value = "granularity")
    @Override
    public String getType() {
        return name().toLowerCase(Locale.ENGLISH);
    }
}
