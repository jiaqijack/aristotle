/*
 * Copyright 刘珈奇
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jiaqijack.druid.model.granularity;

import static com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.ReadablePeriod;

import java.util.Objects;

/**
 * Period granularities are specified as arbitrary period combinations of years, months, weeks, minutes and seconds
 * <p>
 * For example: P2W, P3M, PT1H30M in ISO8601 format. They support specifying a time zone which determines where period
 * boundaries start as well as the timezone of the returned timestamps. By default, years start on the first of Jan.
 * Months start on the first of the month and weeks start on Mon. unless an origin is specified.
 * <p>
 * Time zone is optional (defaults to UTC). Origin is optional (defaults to 1970-01-01T00:00:00 in the given time zone).
 * <p>
 * This will bucket by 2-day chunks in the Pacific timezone.
 * <pre>
 * {"type": "period", "period": "P2D", "timeZone": "America/Los_Angeles"}
 * </pre>
 * This will bucket by 3-month chunks in the Pacific timezone where the 3-month quarters are defined as starting from
 * Feb.
 * <pre>
 * {"type": "period", "period": "P3M", "timeZone": "America/Los_Angeles", "origin": "2012-02-01T00:00:00-08:00"}
 * </pre>
 * Suppose you have data below stored in Druid with millisecond ingestion granularity
 * <pre>
 *     {"timestamp": "2013-08-31T01:02:33Z", "page": "AAA", "language" : "en"}
 *     {"timestamp": "2013-09-01T01:02:33Z", "page": "BBB", "language" : "en"}
 *     {"timestamp": "2013-09-02T23:32:45Z", "page": "CCC", "language" : "en"}
 *     {"timestamp": "2013-09-03T03:32:45Z", "page": "DDD", "language" : "en"}
 * </pre>
 * If you submit a groupBy query with 1-day period in Pacific timezone,
 * <pre>
 * {
 *     "queryType":"groupBy",
 *     "dataSource":"my_dataSource",
 *     "granularity":{"type": "period", "period": "P1D", "timeZone": "America/Los_Angeles"},
 *     "dimensions":[
 *         "language"
 *     ],
 *     "aggregations":[ {
 *          "type":"count",
 *          "name":"count"
 *      } ],
 *     "intervals":[
 *         "1999-12-31T16:00:00.000-08:00/2999-12-31T16:00:00.000-08:00"
 *     ]
 * }
 * </pre>
 * you will get
 * <pre>
 * [
 *     {
 *         "version" : "v1",
 *         "timestamp" : "2013-08-30T00:00:00.000-07:00",
 *         "event" : {
 *             "count" : 1,
 *             "language" : "en"
 *         }
 *     },
 *     {
 *         "version" : "v1",
 *         "timestamp" : "2013-08-31T00:00:00.000-07:00",
 *         "event" : {
 *             "count" : 1,
 *             "language" : "en"
 *         }
 *     },
 *     {
 *         "version" : "v1",
 *         "timestamp" : "2013-09-02T00:00:00.000-07:00",
 *         "event" : {
 *             "count" : 2,
 *             "language" : "en"
 *         }
 *     }
 * ]
 * </pre>
 * Note that the timestamp for each bucket has been converted to Pacific time.
 * Rows {@code {"timestamp": "2013-09-02T23:32:45Z", "page": "CCC", "language" : "en"}} and
 * {@code {"timestamp": "2013-09-03T03:32:45Z", "page": "DDD", "language" : "en"}} are put in the same bucket because
 * they are in the same day in Pacific time. However, the {@code intervals} in groupBy query will not be converted to
 * the specified timezone. The timezone specified in granularity is only applied on the query results. If you set the
 * origin for the granularity to {@code 1970-01-01T20:30:00-08:00}
 * <pre>
 * "granularity": {
 *     "type": "period",
 *     "period": "P1D",
 *     "timeZone": "America/Los_Angeles",
 *     "origin": "1970-01-01T20:30:00-08:00"
 * }
 * </pre>
 * you will get
 * <pre>
 * [
 *     {
 *         "version" : "v1",
 *         "timestamp" : "2013-08-29T20:30:00.000-07:00",
 *         "event" : {
 *             "count" : 1,
 *             "language" : "en"
 *         }
 *     },
 *     {
 *         "version" : "v1",
 *         "timestamp" : "2013-08-30T20:30:00.000-07:00",
 *         "event" : {
 *             "count" : 1,
 *             "language" : "en"
 *         }
 *     },
 *     {
 *         "version" : "v1",
 *         "timestamp" : "2013-09-01T20:30:00.000-07:00",
 *         "event" : {
 *             "count" : 1,
 *             "language" : "en"
 *         }
 *      },
 *      {
 *         "version" : "v1",
 *         "timestamp" : "2013-09-02T20:30:00.000-07:00",
 *         "event" : {
 *             "count" : 1,
 *             "language" : "en"
 *         }
 *     }
 * ]
 * </pre>
 * Note that the {@code origin} you specified has nothing to do with the timezone, it only serves as a starting point
 * for locating the very first granularity bucket. In this case, Rows
 * {@code {"timestamp": "2013-09-02T23:32:45Z", "page": "CCC", "language" : "en"}} and
 * {@code {"timestamp": "2013-09-03T03:32:45Z", "page": "DDD", "language" : "en"}} are not in the same bucket.
 * <p>
 * Timezone support is provided by Joda Time library, which uses the standard IANA time zones.
 */
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({ "type", "period", "timeZone", "origin" })
public class PeriodGranularity implements Granularity {

    /**
     * The type name of period granularity.
     */
    private static final String TYPE = "period";

    /**
     * Error message to use when granularity sees a {@code null} period.
     */
    private static final String NULL_PERIOD_ERROR_MESSAGE = "'period' attribute for PeriodGranularity cannot be null";

    /**
     * The period of each time bucket in Druid data response.
     */
    private final ReadablePeriod period;

    /**
     * A time zone which determines where period boundaries start as well as the timezone of the returned timestamps.
     */
    private final DateTimeZone timeZone;

    /**
     * The starting instance of the each time bucket in Druid data response.
     */
    private final DateTime origin;

    /**
     * Constructs a new {@code PeriodGranularity} with the specified period.
     * <p>
     * The timeZone and origin attributes of this granularity default to null and won't be included in serialization.
     *
     * @param period the period of each time bucket in Druid data response
     */
    public PeriodGranularity(final ReadablePeriod period) {
        this(period, null, null);
    }

    /**
     * Constructs a new {@code PeriodGranularity} with the specified period, time zone, and origin attributes.
     * <p>
     * Either the time zone and origin can be {@code null}; in these cases, they will be assigned values whatever Druid
     * decides on. If both time zone and origin are not {@code null}, the origin will have the passed-in time zone.
     *
     * @param period the period of each time bucket in Druid data response
     * @param timeZone a time zone which determines where period boundaries start as well as the timezone of the
     * returned timestamps
     * @param origin the starting instance of the each time bucket in Druid data response
     */
    public PeriodGranularity(final ReadablePeriod period, final DateTimeZone timeZone, final DateTime origin) {
        Objects.requireNonNull(period, NULL_PERIOD_ERROR_MESSAGE);
        this.period = period;
        this.timeZone = timeZone;
        if (timeZone != null && origin != null) {
            this.origin = origin.withZone(timeZone);
        } else {
            this.origin = origin;
        }
    }

    @JsonProperty(value = "type")
    @Override
    public String getType() {
        return TYPE;
    }

    /**
     * Returns the period attribute of this granularity used for JSON serialization.
     * <p>
     * This method calls {@link #getPeriod()} as a source of string attribute.
     *
     * @return granularity period for JSON serialization
     */
    @JsonProperty(value = "period")
    public String getStringPeriod() {
        return getPeriod().toString();
    }

    /**
     * Returns the time zone attribute of this granularity used for JSON serialization.
     * <p>
     * This method calls {@link #getTimeZone()} as a source of string attribute. If time zone is not specified for this
     * granularity, the method returns {@code null}.
     *
     * @return granularity time zone for JSON serialization
     */
    @JsonProperty(value = "timeZone")
    public String getTimeZoneName() {
        final DateTimeZone source = getTimeZone();
        return source == null ? null : source.toString();
    }

    /**
     * Returns the origin attribute of this granularity used for JSON serialization.
     * <p>
     * This method calls {@link #getOrigin()} as a source of string attribute. If origin is not specified for this
     * granularity, the method returns {@code null}.
     *
     * @return granularity origin for JSON serialization
     */
    @JsonProperty(value = "origin")
    public String getOriginString() {
        final DateTime dateTime = getOrigin();
        return dateTime == null ? null : dateTime.toString();
    }

    /**
     * Returns period attribute of this granularity as a {@link ReadablePeriod} object.
     *
     * @return the granularity period as a {@link ReadablePeriod} object
     */
    public ReadablePeriod getPeriod() {
        return period;
    }

    /**
     * Returns time zone attribute of this granularity as a {@link DateTimeZone} object.
     *
     * @return the granularity time zone as a {@link DateTimeZone} object
     */
    public DateTimeZone getTimeZone() {
        return timeZone;
    }

    /**
     * Returns origin attribute of this granularity as a {@link DateTime} object.
     *
     * @return the granularity origin as a {@link DateTime} object
     */
    public DateTime getOrigin() {
        return origin;
    }

    /**
     * Returns the string representation of this granularity.
     * <p>
     * The format of the string is "PeriodGranularity{period=XXX, timeZone=YYY, origin=ZZZ}", where XXX is given by
     * {@link #getPeriod()}, YYY by {@link #getTimeZone()}, and ZZZ by {@link #getOrigin()}. Note that each value is
     * separated by a comma followed by a single space. Values are not surrounded by single quotes.
     *
     * @return the string representation of this granularity
     */
    @Override
    public String toString() {
        return String.format(
                "PeriodGranularity{period=%s, timeZone=%s, origin=%s}",
                getPeriod(),
                getTimeZone(),
                getOrigin()
        );
    }
}
