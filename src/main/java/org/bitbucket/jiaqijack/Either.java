/*
 * Copyright 刘珈奇
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jiaqijack;

import static org.bitbucket.jiaqijack.jaxrs.config.ErrorMessageFormat.EITHER_ERROR_LEFT_OF_RIGHT;
import static org.bitbucket.jiaqijack.jaxrs.config.ErrorMessageFormat.EITHER_ERROR_RIGHT_OF_LEFT;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * An {@link org.bitbucket.jiaqijack.Either} is a bean that contains one of two possible values, referred to generically
 * as Left or Right.
 * <p>
 * An {@link org.bitbucket.jiaqijack.Either} is _not_ a pair. It does not contain 2 value. It only contains 1 value.
 * However, the type of the value could either be the left type or the right type.
 *
 * @param <L>  The type of the Left value
 * @param <R>  The type of the Right value
 */
public class Either<L, R> {
    /**
     * Logger.
     */
    private static final Logger LOG = LoggerFactory.getLogger(Either.class);

    /**
     * The Left value, this should be null if isLeft is false.
     */
    private final L left;
    /**
     * The Right value, this should be null if isLeft is true.
     */
    private final R right;
    /**
     * True if this is a Left value or false if this is a Right value.
     */
    private final boolean isLeft;

    /**
     * Constructor.
     * <p>
     * Builds an object that is either of type L, or type R.
     *
     * @param left  The left value, this should be null if isLeft is false
     * @param right  The right value, this should be null if isLeft is true
     * @param isLeft  Whether or not this is a right or left value
     */
    private Either(final L left, final R right, final boolean isLeft) {
        this.left = left;
        this.right = right;
        this.isLeft = isLeft;
    }

    /**
     * Constructs an {@link org.bitbucket.jiaqijack.Either} containing a left value.
     *
     * @param left  The left value to store in the {@link org.bitbucket.jiaqijack.Either}
     * @param <L>  The type of the left value
     * @param <R>  The type of the right value
     *
     * @return an {@link org.bitbucket.jiaqijack.Either} wrapping the left value
     */
    public static <L, R> Either<L, R> left(final L left) {
        return new Either<>(left, null, true);
    }

    /**
     * Constructs an {@link org.bitbucket.jiaqijack.Either} containing a right value.
     *
     * @param right  The right value to store in the {@link org.bitbucket.jiaqijack.Either}
     * @param <L>  The type of the left value
     * @param <R>  The type of the right value
     *
     * @return an {@link org.bitbucket.jiaqijack.Either} wrapping the right value
     */
    public static <L, R> Either<L, R> right(final R right) {
        return new Either<>(null, right, false);
    }

    /**
     * Returns whether or not this {@link org.bitbucket.jiaqijack.Either} represents a Left value.
     *
     * @return true if this {@link org.bitbucket.jiaqijack.Either} is a Left value or false, otherwise
     */
    public boolean isLeft() {
        return isLeft;
    }

    /**
     * Returns whether or not this {@link org.bitbucket.jiaqijack.Either} represents a Right value.
     *
     * @return true if this {@link org.bitbucket.jiaqijack.Either} is a Right value or false, otherwise
     */
    public boolean isRight() {
        return !isLeft;
    }

    /**
     * Returns the Left value wrapped in this {@link org.bitbucket.jiaqijack.Either}.
     *
     * @return the Lefe value wrapped in this {@link org.bitbucket.jiaqijack.Either}
     *
     * @throws UnsupportedOperationException if this {@link org.bitbucket.jiaqijack.Either} wraps a Right value instead
     */
    public L getLeft() {
        if (!isLeft) {
            LOG.error(String.format(EITHER_ERROR_LEFT_OF_RIGHT.getLoggingFormat(), this));
            throw new UnsupportedOperationException(EITHER_ERROR_LEFT_OF_RIGHT.format(this));
        }
        return left;
    }

    /**
     * Returns the Right value wrapped in this {@link org.bitbucket.jiaqijack.Either}.
     *
     * @return the Right value wrapped in this {@link org.bitbucket.jiaqijack.Either}
     *
     * @throws UnsupportedOperationException if this {@link org.bitbucket.jiaqijack.Either} wraps a Left value instead
     */
    public R getRight() {
        if (isLeft()) {
            LOG.error(String.format(EITHER_ERROR_RIGHT_OF_LEFT.getLoggingFormat(), this));
            throw new UnsupportedOperationException(EITHER_ERROR_RIGHT_OF_LEFT.format(this));
        }
        return right;
    }
}
