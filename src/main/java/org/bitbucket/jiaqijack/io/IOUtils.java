/*
 * Copyright 刘珈奇
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jiaqijack.io;

import org.bitbucket.jiaqijack.annotation.NotNull;

import net.jcip.annotations.Immutable;
import net.jcip.annotations.ThreadSafe;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;

/**
 * Assorted methods for handling I/O operations.
 */
@Immutable
@ThreadSafe
@SuppressWarnings("AbbreviationAsWordInName")
public final class IOUtils {

    /**
     * Constructor.
     * <p>
     * Suppress default constructor for noninstantiability.
     */
    private IOUtils() {
        throw new AssertionError();
    }

    /**
     * Read contents of a specified file into a single String.
     *
     * @param path  The absolute path of the specified file
     *
     * @return file content in a single string
     *
     * @throws IllegalArgumentException  if an I/O error occurs reading from the file
     * @throws NullPointerException if the absolute path is {@code null}
     */
    @NotNull
    public static String readFileAsString(@NotNull final String path) {
        Objects.requireNonNull(path, "path");

        try {
            return new String(Files.readAllBytes(Paths.get(path)));
        } catch (final IOException exception) {
            throw new IllegalArgumentException(String.format("Error on reading '%s'", path), exception);
        }
    }

    /**
     * Prints to the terminal a specified stream containing error.
     *
     * @param in  The error stream to be printed
     *
     * @throws IOException If the first byte cannot be read for any reason other than the end of the file, if the input
     * stream has been closed, or if some other I/O error occurs.
     * @throws NullPointerException if any argument {@code in} is {@code null}
     */
    public static void printErrorStream(@NotNull final InputStream in) throws IOException {
        printStream(System.err, in);
    }

    /**
     * Prints a specified stream to a specified {@link PrintStream} object.
     *
     * @param printer  The provided {@link PrintStream} object
     * @param in  The stream to be printed
     *
     * @throws IOException If the first byte cannot be read for any reason other than the end of the file, if the input
     * stream has been closed, or if some other I/O error occurs.
     * @throws NullPointerException if any argument is {@code null}
     */
    public static void printStream(@NotNull final PrintStream printer, @NotNull final InputStream in)
            throws IOException {
        Objects.requireNonNull(printer, "printer");
        Objects.requireNonNull(in, "in");

        final byte[] buffer = new byte[1024];
        for (int i = in.read(buffer); i != -1; i = in.read(buffer)) {
            printer.write(buffer, 0, i);
        }
    }
}
