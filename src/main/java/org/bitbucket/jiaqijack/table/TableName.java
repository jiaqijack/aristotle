/*
 * Copyright 刘珈奇
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jiaqijack.table;

import java.util.Comparator;

/**
 * Marker interface for objects that can be treated as a table name.
 */
public interface TableName {

    /**
     * Comparator to order TableNames by their asName methods, using the native String comparator.
     */
    Comparator<TableName> COMPARATOR = Comparator.comparing(TableName::asName);

    /**
     * Returns a string representation of a table name.
     *
     * @return the table name
     */
    String asName();

    /**
     * Wraps a string in an anonymous instance of TableName and returns it.
     *
     * @param name the name being wrapped
     *
     * @return an anonymous sub-class instance of TableName
     */
    static TableName of(final String name) {
        return new TableName() {
            @Override
            public String asName() {
                return name;
            }

            @Override
            public String toString() {
                return asName();
            }
        };
    }
}
