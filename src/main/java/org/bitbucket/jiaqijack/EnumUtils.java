/*
 * Copyright 刘珈奇
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jiaqijack;

import static org.bitbucket.jiaqijack.jaxrs.config.ErrorMessageFormat.NOT_ALTERNATE_KEY_FOR_ENUM;

import java.util.Locale;
import java.util.Map;

/**
 * Helper methods for enums.
 */
public class EnumUtils {
    /**
     * Boilerplate method to retrieve an enum method via an arbitrary key, rather than just the enum value.
     *
     * @param <T>  Enum we are getting the value by key for
     * @param <K>  Type of the key to get the enum value for
     * @param key  Key to get the enum value for
     * @param mapping  Mapping to use to look up the value by key
     * @param enumeration  Enum we are getting value by key for (needed due to type erasure)
     *
     * @return The enum value for the key
     * @throws IllegalArgumentException if this enum type has no constant with the specified name
     */
    public static <T extends Enum<T>, K> T forKey(final K key, final Map<K, T> mapping, final Class<T> enumeration) {
        final T t = mapping.get(key);
        if (t != null) {
            return t;
        }
        throw new IllegalArgumentException(NOT_ALTERNATE_KEY_FOR_ENUM.format(enumeration, key));
    }

    /**
     * Converts the enum value to a camel case string e.g. ENUM_CONST_VALUE {@literal ->} enumConstValue.
     *
     * @param e  The enum value to be converted
     *
     * @return enum string changed to camel case format
     */
    public static String enumJsonName(final Enum<?> e) {
        return camelCase(e.name());
    }

    /**
     * Converts the string value to a camel case string. e.g. ENUM_CONST_VALUE {@literal ->} enumConstValue.
     *
     * @param string  The string to be converted
     *
     * @return string changed to camel case format
     */
    public static String camelCase(final String string) {
        final String[] words = string.toLowerCase(Locale.ENGLISH).split("_");
        final StringBuilder lowerCamelCase = new StringBuilder(words[0]);

        for (int i = 1; i < words.length; i++) {
            lowerCamelCase.append(words[i].substring(0, 1).toUpperCase(Locale.ENGLISH));
            lowerCamelCase.append(words[i].substring(1));
        }

        return lowerCamelCase.toString();
    }
}
