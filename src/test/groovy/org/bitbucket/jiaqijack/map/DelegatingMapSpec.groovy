/*
 * Copyright 刘珈奇
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jiaqijack.map

import spock.lang.Specification

class DelegatingMapSpec extends Specification {
    Map innermostMap
    DelegatingMap midMap
    DelegatingMap topMap

    def setup() {
        innermostMap = [a: 3, b: 3, c: 3, e: 3] as LinkedHashMap

        midMap = new DelegatingMap(innermostMap)
        midMap.putAll([a: 2, b: 2, d: 2])

        topMap = new DelegatingMap(midMap)
        topMap.putAll([a: 1, e: 1, f: 1])
    }

    def "Delegation returns value from correct level"() {
        expect:
        topMap.get('a') == 1
        topMap.get('b') == 2
        topMap.get('c') == 3
        topMap.get('d') == 2
        topMap.get('e') == 1
        topMap.get('f') == 1
        topMap.get('g') == null
    }

    def "Clear throws exception"() {
        when:
        topMap.clear()

        then:
        thrown(UnsupportedOperationException)
    }

    def "ClearLocal wipes out all top level entries"() {
        when:
        topMap.clearLocal()

        then:
        topMap.get('a') == 2
        topMap.get('b') == 2
        topMap.get('c') == 3
        topMap.get('d') == 2
        topMap.get('e') == 3
        topMap.get('f') == null
        topMap.get('g') == null
    }

    def "ContainsKey corresponds to visible entry keys"() {
        expect:
        topMap.containsKey('a')
        topMap.containsKey('b')
        topMap.containsKey('c')
        topMap.containsKey('d')
        topMap.containsKey('e')
        topMap.containsKey('f')
        !topMap.containsKey('g')
    }

    def "ContainsValue returns available values"() {
        expect:
        topMap.containsValue(1)
        topMap.containsValue(2)
        topMap.containsValue(3)

        !midMap.containsValue(1)
        midMap.containsValue(2)
        midMap.containsValue(3)

        when:
        midMap.clearLocal()

        then:
        topMap.containsValue(1)
        !topMap.containsValue(2)
        topMap.containsValue(3)

        !midMap.containsValue(1)
        !midMap.containsValue(2)
        midMap.containsValue(3)

        when:
        topMap.clearLocal()

        then:
        !topMap.containsValue(1)
        !topMap.containsValue(2)
        topMap.containsValue(3)
    }

    def "Keyset has right values in right order"() {
        expect:
        topMap.keySet() as List == ['c', 'b', 'd', 'a', 'e', 'f']
    }

    def "Entry stream is in canonical order"() {
        expect:
        topMap.entrySet().collect {[it.key, it.value]} == [['c', 3], ['b', 2], ['d', 2], ['a', 1], ['e', 1], ['f', 1]]
    }

    def "Put correctly overshadows"() {
        setup:
        int oldValue = topMap.put("c", 4)

        expect:
        oldValue == 3
        topMap.get('c') == 4
    }

    def "Remove throws exception"() {
        when:
        topMap.remove('a')

        then:
        thrown(UnsupportedOperationException)
    }

    def "RemoveLocal reveals shadowed values from delegate"() {
        setup:
        topMap.removeLocal('a')

        expect:
        topMap.get('a') == 2
    }

    def "Size increases and decreases based on count of top level keys"() {
        expect: "Default size"
        topMap.size() == 6

        when: "Overshadowing doesn't increase size"
        topMap.put('b', 2)

        then:
        topMap.size() == 6

        when: "Removing an unshadowed value lower the size"
        topMap.removeLocal('f')

        then:
        topMap.size() == 5

        when: "Adding an unshadowed value increase size"
        topMap.put('z', 12)

        then:
        topMap.size() == 6
    }

    def "Values contains correct values"() {
        expect:
        topMap.values() == [1, 2, 3] as Set
    }

    def "Flat view has a view of data"() {
        expect:
        topMap.flatView() == [c: 3, b: 2, d: 2, a: 1, e: 1, f: 1]
    }
}
