/*
 * Copyright 刘珈奇
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jiaqijack.druid.model.granularity

import com.fasterxml.jackson.databind.ObjectMapper

import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.Period

import spock.lang.Specification
import spock.lang.Unroll

class PeriodGranularitySpec extends Specification {

    static final ObjectMapper OBJECT_MAPPER = new ObjectMapper()
    static final LA_TIMEZONE = DateTimeZone.forID("America/Los_Angeles")
    static final LA_ORIGIN = new DateTime("2012-02-01T20:30:00-08:00")

    def "Null period throws error"() {
        when:
        new PeriodGranularity(null, null, null)

        then:
        Exception exception = thrown()
        exception instanceof NullPointerException
        exception.message == PeriodGranularity.NULL_PERIOD_ERROR_MESSAGE
    }

    @Unroll
    def "#period with in #timeZone with origin of #origin serializes to #exptectedJson"() {
        expect:
        OBJECT_MAPPER.writeValueAsString(
                new PeriodGranularity(Period.parse(period), timeZone, origin)
        ) == exptectedJson

        where:
        period    | timeZone         | origin    || exptectedJson
        "P2W"     | null             | null      || '{"type":"period","period":"P2W"}'
        "P3M"     | null             | null      || '{"type":"period","period":"P3M"}'
        "PT1H30M" | null             | null      || '{"type":"period","period":"PT1H30M"}'
        "P2W"     | DateTimeZone.UTC | null      || '{"type":"period","period":"P2W","timeZone":"UTC"}'
        "P3M"     | LA_TIMEZONE      | LA_ORIGIN || '{"type":"period","period":"P3M","timeZone":"America/Los_Angeles","origin":"2012-02-01T20:30:00.000-08:00"}'
    }

    @Unroll
    def "Granularity with #timeZone timeZone serializes to #exptectedJson"() {
        expect:
        OBJECT_MAPPER.writeValueAsString(
                new PeriodGranularity(Period.parse("P1D"), DateTimeZone.forID(timeZone), null)
        ) == exptectedJson

        where:
        timeZone << DateTimeZone.availableIDs
        exptectedJson = """{"type":"period","period":"P1D","timeZone":"${DateTimeZone.forID(timeZone).toString()}"}"""
    }
}
