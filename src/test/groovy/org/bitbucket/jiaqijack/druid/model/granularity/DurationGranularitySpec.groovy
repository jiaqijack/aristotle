/*
 * Copyright 刘珈奇
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jiaqijack.druid.model.granularity

import com.fasterxml.jackson.databind.ObjectMapper

import org.joda.time.DateTime
import org.joda.time.Duration

import spock.lang.Specification
import spock.lang.Unroll

class DurationGranularitySpec extends Specification {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper()

    @Unroll
    def "#duration milliseconds with origin of #origin serializes to #exptectedJson with default timezone (UTC)"() {
        expect:
        OBJECT_MAPPER.writeValueAsString(
                origin == null
                        ? new DurationGranularity(new Duration(duration))
                        : new DurationGranularity(new Duration(duration), new DateTime(origin)
                )
        ) == exptectedJson

        where:
        duration | origin                 || exptectedJson
        7200000  | null                   || '{"type":"duration","duration":7200000}'
        3600000  | null                   || '{"type":"duration","duration":3600000}'
        7200000  | "2012-01-01T00:30:00Z" || '{"type":"duration","duration":7200000,"origin":"2012-01-01T00:30:00.000Z"}'
        3600000  | "2012-01-01T00:30:00Z" || '{"type":"duration","duration":3600000,"origin":"2012-01-01T00:30:00.000Z"}'
    }
}
