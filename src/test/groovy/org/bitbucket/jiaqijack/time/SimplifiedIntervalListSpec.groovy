/*
 * Copyright 刘珈奇
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jiaqijack.time

import org.joda.time.DateTime
import org.joda.time.Days
import org.joda.time.Interval

import spock.lang.Specification
import spock.lang.Unroll

class SimplifiedIntervalListSpec extends Specification {
    static List<List<Integer>> tinyEvenIntervals
    static List<List<Integer>> tinyOddIntervals
    static List<List<Integer>> mediumIntervals
    static List<List<Integer>> largeEvenIntervals

    def setupSpec() {
        tinyEvenIntervals = [[2, 4], [6, 10], [14, 30]]
        tinyOddIntervals = [[1, 3], [7, 9], [11, 15], [17, 19]]
        mediumIntervals = [[5, 11], [16, 21]]
        largeEvenIntervals = [[4, 42]]
    }

    @Unroll
    def "SimplifyIntervals(Collection<Interval>...) returns #expected with input #input"() {
        expect:
        SimplifiedIntervalList.simplifyIntervals(buildIntervalList(input)) == buildIntervalList(expected)

        where:
        input                                                 | expected
        ["2014/2017", "2015/2020"]                            | ["2014/2020"]
        ["2014/2020"]                                         | ["2014/2020"]
        ["2015/2016", "2013/2014"]                            | ["2013/2014", "2015/2016"]
        ["2015/2015", "2015/2016", "2012/2013"]               | ["2012/2013", "2015/2016"]
        ["2015-01/2015-02", "2015-02/2015-03"]                | ["2015-01/2015-03"]
        ["2015-01-14T10:00:00.000Z/2015-01-15T10:00:00.000Z",
         "2015-01-15T10:00:00.000Z/2015-01-16T10:00:00.000Z"] | ["2015-01-14T10:00:00.000Z/2015-01-16T10:00:00.000Z"]
    }

    @Unroll
    def "appendWithMerge(Interval) produces #expected starting from #original with interval #addend"() {
        given: "a original simplified interval list that is to be appended"
        List<Interval> workingList = new SimplifiedIntervalList(buildIntervalList(original))

        when: "a new interval is appended to the original list"
        workingList.appendWithMerge(new Interval(addend))

        then: "the interval is merged into the original list"
        workingList == new SimplifiedIntervalList(buildIntervalList(expected))

        where:
        original | addend | expected
        ["2015/2016"]                           | "2014/2017" | ["2014/2017"]
        ["2015/2016"]                           | "2016/2020" | ["2015/2020"]
        ["2015/2018"]                           | "2016/2020" | ["2015/2020"]
        ["2015/2017"]                           | "2018/2020" | ["2015/2017", "2018/2020"]
        ["2010/2011", "2014/2018"]              | "2015/2016" | ["2010/2011", "2014/2018"]
        ["2010/2012", "2013/2014"]              | "2009/2012" | ["2009/2012", "2013/2014"]
        ["2010/2012", "2015/2016"]              | "2013/2014" | ["2010/2012", "2013/2014", "2015/2016"]
        ["2010/2012", "2015/2016", "2017/2018"] | "2012/2015" | ["2010/2016", "2017/2018"]
        ["2010/2012", "2015/2016", "2017/2018"] | "2013/2014" | ["2010/2012", "2013/2014", "2015/2016", "2017/2018"]
        ["2010/2011", "2014/2015", "2016/2017",
         "2018/2020", "2021/2022"]              | "2015/2021" | ["2010/2011", "2014/2022"]
    }

    @Unroll
    def "Union of #left and #right yields #expected"() {
        given: "a two simplified interval lists and a expected simplified interval list"
        SimplifiedIntervalList expectedList = buildIntervalListNum(expected)
        SimplifiedIntervalList leftList = buildIntervalListNum(left)
        SimplifiedIntervalList rightList = buildIntervalListNum(right)

        expect: "the two unions together to get the expected simplified interval list"
        leftList.union(rightList) == expectedList
        rightList.union(leftList) == expectedList

        where:
        left              | right              | expected
        tinyEvenIntervals | tinyOddIntervals   | [[1, 4], [6, 10], [11, 30]]
        tinyEvenIntervals | mediumIntervals    | [[2, 4], [5, 11], [14, 30]]
        tinyOddIntervals  | mediumIntervals    | [[1, 3], [5, 15], [16, 21]]
        tinyEvenIntervals | tinyEvenIntervals  | tinyEvenIntervals
        tinyEvenIntervals | largeEvenIntervals | [[2,42]]
        []                | tinyEvenIntervals  | tinyEvenIntervals
        []                | []                 | []
    }

    @Unroll
    def "Intersection of #left and #right yields #expected"() {
        setup: "a two simplified interval lists and a expected simplified interval list"
        SimplifiedIntervalList expectedList = buildIntervalListNum(expected)
        SimplifiedIntervalList leftList = buildIntervalListNum(left)
        SimplifiedIntervalList rightList = buildIntervalListNum(right)

        expect: "the two intersects together to get the expected simplified interval list"
        leftList.intersect(rightList) == expectedList
        rightList.intersect(leftList) == expectedList

        where:
        left              | right              | expected
        tinyEvenIntervals | tinyOddIntervals   | [[2,3], [7,9], [14, 15], [17, 19]]
        tinyEvenIntervals | mediumIntervals    | [[6, 10], [16, 21]]
        tinyOddIntervals  | mediumIntervals    | [[7, 9], [17, 19]]
        tinyEvenIntervals | tinyEvenIntervals  | tinyEvenIntervals
        tinyEvenIntervals | largeEvenIntervals | [[6,10], [14, 30]]
        []                | tinyOddIntervals   | []
    }

    @Unroll
    def "Subtraction of #left and #right yields #expected"() {
        setup: "a two simplified interval lists and a expected simplified interval list"
        SimplifiedIntervalList expectedList = buildIntervalListNum(expected)
        SimplifiedIntervalList leftList = buildIntervalListNum(left)
        SimplifiedIntervalList rightList = buildIntervalListNum(right)

        expect: "the first subtracts the second to get the expected simplified interval list"
        leftList.subtract(rightList) == expectedList

        where:
        left              | right              | expected
        tinyEvenIntervals | tinyOddIntervals   | [[3, 4], [6, 7], [9,10], [15, 17], [19, 30]]
        tinyOddIntervals  | tinyEvenIntervals  | [[1, 2], [11, 14]]
        tinyEvenIntervals | mediumIntervals    | [[2, 4], [14, 16], [21, 30]]
        tinyOddIntervals  | mediumIntervals    | [[1, 3], [11, 15]]
        tinyEvenIntervals | tinyEvenIntervals  | []
        tinyEvenIntervals | largeEvenIntervals | [[2, 4]]
        tinyEvenIntervals | []                 | tinyEvenIntervals
        []                | tinyOddIntervals   | []
        []                | []                 | []
    }

    @Unroll
    def "Period Iterator creates period sliced starting #expected when dividing #rawIntervals by #period"() {
        given: "an expected periods iterator"
        Iterator<Interval> expectedIterator  = expected.collect { new Interval(new DateTime(it), period) }.iterator()

        when: "#rawIntervals are divided by #period"
        Iterator<Interval> actualIterator = new SimplifiedIntervalList(
                rawIntervals.collect { new Interval(new DateTime(it[0]), it[1]) }
        ).periodIterator(period)

        then: "the results match expected periods"
        while (expectedIterator.hasNext()) {
            assert expectedIterator.next() == actualIterator.next()
        }
        !actualIterator.hasNext()

        where:
        period     | rawIntervals                               | expected
        Days.ONE   | [["2015", Days.THREE]]                     | ["2015-01-01", "2015-01-02", "2015-01-03"]
        Days.THREE | [["2015", Days.THREE]]                     | ["2015-01-01"]
        Days.ONE   | [["2015", Days.THREE], ["2013", Days.ONE]] | ["2013-01-01", "2015-01-01", "2015-01-02",
                                                                   "2015-01-03"]
    }

    /**
     * Constructs a list of Interval instances whose values are given by a list of strings.
     *
     * @param intervals  The list of strings
     *
     * @return the list of Interval instaces
     */
    List buildIntervalList(Collection<String> intervals) {
        intervals.collect { new Interval(it) }
    }

    /**
     * Constructs a list of Interval instances whose values are given by a list of start-end pairs.
     *
     * @param times  The give list of start-end pairs
     *
     * @return the list of Interval instances
     */
    List buildIntervalListNum(List<List<Long>> times) {
        times.collect { new Interval(it[0], it[1]) }
    }
}
