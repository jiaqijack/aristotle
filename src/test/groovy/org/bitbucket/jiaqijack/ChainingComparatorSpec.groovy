/*
 * Copyright 刘珈奇
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jiaqijack

import spock.lang.Specification
import spock.lang.Unroll

class ChainingComparatorSpec extends Specification {
    static Comparator<Integer> zero
    static Comparator<Integer> less
    static Comparator<Integer> more

    def setupSpec() {
        zero = Mock(Comparator)
        less = Mock(Comparator)
        more = Mock(Comparator)

        zero.compare(_, _) >> 0
        less.compare(_, _) >> -1
        more.compare(_, _) >> 1
    }

    @Unroll
    def "Test chain comparator with #comparatorList returns #expected"() {
        setup:
        ChainingComparator<Integer> chainingComparator = new ChainingComparator<>(comparatorList)

        expect:
        chainingComparator.compare(1, 2) == expected

        where:
        comparatorList     | expected
        [less]             | -1
        [zero]             |  0
        [more]             |  1
        [less, zero, more] | -1
        [more, zero, less] |  1
        [zero, more, less] |  1
        [zero, less, more] | -1
        [zero, zero, zero] |  0
        []                 |  0
    }
}
