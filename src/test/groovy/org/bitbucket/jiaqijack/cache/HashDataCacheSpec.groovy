/*
 * Copyright 刘珈奇
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jiaqijack.cache

import spock.lang.Specification
import spock.lang.Subject

import java.security.MessageDigest

class HashDataCacheSpec extends Specification {

    static final String HASH_ALGORITHM = "SHA-512"
    static final MessageDigest MESSAGE_DIGEST = MessageDigest.getInstance(HASH_ALGORITHM)

    @Subject
    DataCache<String, String> cache
    DataCache<String, HashDataCache.Pair<String>> innerCache


    @SuppressWarnings(["GroovyAccessibility"])
    def setup() {
        innerCache = Mock(DataCache)
        cache = new HashDataCache<>(innerCache, MESSAGE_DIGEST)
    }

    @SuppressWarnings(["GroovyAccessibility", "GroovyResultOfObjectAllocationIgnored"])
    def "While constructing cache, its underlying cache cannot be null"() {
        when: "the object used to hold cache is null"
        new HashDataCache<>(null, MESSAGE_DIGEST)

        then: "data cache object is not instantiated"
        Exception exception = thrown(NullPointerException)
        exception.message == "cache"

        when: "invoking the other constructor with the same null object"
        new HashDataCache<>(null, HASH_ALGORITHM)

        then: "data cache object is not instantiated neither"
        exception = thrown(NullPointerException)
        exception.message == "cache"
    }

    @SuppressWarnings("GroovyResultOfObjectAllocationIgnored")
    def "While constructing cache, the algorithm name used for constructing hashing algorithm object cannot be null"() {
        when: "the name of the hashing algorithm used to create hashing object is null"
        new HashDataCache<>(Mock(DataCache), (String) null)

        then: "data cache object is not instantiated"
        Exception exception = thrown(NullPointerException)
        exception.message == "algorithm"
    }

    @SuppressWarnings(["GroovyAccessibility", "GroovyResultOfObjectAllocationIgnored"])
    def "While constructing cache, the message digest used for hashing cannot be null"() {
        when: "the hashing object is null"
        new HashDataCache<>(Mock(DataCache), (MessageDigest) null)
        
        then: "data cache object is not instantiated"
        Exception exception = thrown(NullPointerException)
        exception.message == "messageDigest"
    }

    def "Cache throws NPE on getting null key"() {
        when: "a searching by a null cache key"
        cache.get(null)

        then: "the null key is detected and error is thrown"
        Exception exception = thrown(NullPointerException)
        exception.message == "key"
    }

    def "Un-cached key returns null cache value"() {
        expect: "searching for non-existing cache returns null"
        cache.get("foo") == null
    }

    @SuppressWarnings(["GroovyAccessibility"])
    def "Getting from cache the existing value returns that value"() {
        given: "an existing cache key"
        innerCache.get(HashDataCache.hash(MESSAGE_DIGEST, "foo")) >> new HashDataCache.Pair<String>("foo", "bar")

        expect: "the value of that key is retrieved from cache"
        cache.get("foo") == "bar"
    }

    @SuppressWarnings(["GroovyAccessibility"])
    def "When lookup up a key results in hash collision, null is returnd from cache"() {
        given: "a key resolves to a HashDataCache.Pair that has a different key but with the same hash"
        innerCache.get(HashDataCache.hash(MESSAGE_DIGEST, "foo")) >> new HashDataCache.Pair<String>("notFoo", "bar")

        expect: "the key gets null cache value"
        cache.get("foo") == null
    }

    def "Inserting a cache does not allow null key"() {
        when: "cache key is null"
        cache.set(null, "foo")

        then: "the null key is detected and error is thrown"
        Exception exception = thrown(NullPointerException)
        exception.message == "key"
    }

    @SuppressWarnings(["GroovyAccessibility"])
    def "Happy path inserting data into cache"() {
        when: "caching a valid data"
        cache.set("foo", "bar")

        then: "a cache has a new entry"
        1 * innerCache.set(_ as String, _ as HashDataCache.Pair) >> true
    }

    def "Clearing the cache removes all data"() {
        when: "trying to cleanup cache"
        cache.clear()

        then: "the object that holds all cache is cleared"
        1 * innerCache.clear()
    }
}
