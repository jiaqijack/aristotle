<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

    <modelVersion>4.0.0</modelVersion>
    <prerequisites>
        <maven>3.0</maven>
    </prerequisites>

    <groupId>org.bitbucket.jiaqijack</groupId>
    <artifactId>Aristotle</artifactId>
    <packaging>jar</packaging>
    <version>1.36-SNAPSHOT</version>
    <name>Aristotle</name>
    <url>https://bitbucket.org/jiaqijack/aristotle</url>

    <developers>
        <developer>
            <name>刘珈奇</name>
            <url>https://bitbucket.org/jiaqijack</url>
        </developer>
    </developers>

    <licenses>
        <license>
            <name>The Apache Software License, Version 2.0</name>
            <url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
            <distribution>repo</distribution>
        </license>
    </licenses>

    <distributionManagement>
        <repository>
            <id>bintray-jiaqijack-Maven</id>
            <name>jiaqijack-Maven</name>
            <url>https://api.bintray.com/maven/jiaqijack/Maven/Aristotle/;publish=1</url>
        </repository>
    </distributionManagement>

    <scm>
        <connection>scm:git:https://jiaqijack@bitbucket.org/jiaqijack/aristotle.git</connection>
        <developerConnection>scm:git:git@bitbucket.org:jiaqijack/aristotle.git</developerConnection>
        <url>https://bitbucket.org/jiaqijack/aristotle</url>
        <tag>HEAD</tag>
    </scm>

    <properties>
        <!-- Jersey -->
        <jerse.version>2.25.1</jerse.version>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.encoding>UTF-8</maven.compiler.encoding>
    </properties>

    <dependencies>
        <!-- Jersey -->
        <dependency>
            <groupId>org.glassfish.jersey.core</groupId>
            <artifactId>jersey-server</artifactId>
            <version>${jerse.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.glassfish.jersey.containers</groupId>
            <artifactId>jersey-container-servlet-core</artifactId>
            <version>${jerse.version}</version>
            <scope>provided</scope>
        </dependency>

        <!-- Hadoop - Must appear after Jersey dependencies in POM for dependency to load in proper order -->
        <dependency>
            <groupId>org.apache.hadoop</groupId>
            <artifactId>hadoop-common</artifactId>
            <version>3.0.0-alpha1</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.apache.hadoop</groupId>
            <artifactId>hadoop-common</artifactId>
            <version>3.0.0-alpha1</version>
            <classifier>tests</classifier>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.apache.hadoop</groupId>
            <artifactId>hadoop-hdfs</artifactId>
            <version>3.0.0-alpha1</version>
            <classifier>tests</classifier>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.apache.hadoop</groupId>
            <artifactId>hadoop-hdfs</artifactId>
            <version>3.0.0-alpha1</version>
            <scope>test</scope>
        </dependency>

        <!-- Async -->
        <dependency>
            <groupId>io.reactivex.rxjava2</groupId>
            <artifactId>rxjava</artifactId>
            <version>2.1.14</version>
        </dependency>
        <dependency>
            <groupId>org.asynchttpclient</groupId>
            <artifactId>async-http-client</artifactId>
            <version>2.4.8</version>
        </dependency>
        <dependency> <!-- DefaultAsyncHttpClientConfig -->
            <groupId>io.netty</groupId>
            <artifactId>netty-all</artifactId>
            <version>4.1.9.Final</version>
            <scope>test</scope>
        </dependency>

        <!-- GraphQL -->
        <dependency>
            <groupId>com.graphql-java</groupId>
            <artifactId>graphql-java</artifactId>
            <version>13.0</version>
        </dependency>

        <!-- Concurrency -->
        <dependency>
            <groupId>net.jcip</groupId>
            <artifactId>jcip-annotations</artifactId>
            <version>1.0</version>
        </dependency>

        <!-- Caching -->
        <dependency>
            <groupId>net.spy</groupId>
            <artifactId>spymemcached</artifactId>
            <version>2.12.3</version>
        </dependency>

        <!-- JSON -->
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-databind</artifactId>
            <version>2.9.9.3</version>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-annotations</artifactId>
            <version>2.9.6</version>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.dataformat</groupId>
            <artifactId>jackson-dataformat-csv</artifactId>
            <version>2.9.4</version>
        </dependency>
        <dependency>
            <groupId>com.google.code.gson</groupId>
            <artifactId>gson</artifactId>
            <version>2.8.5</version>
        </dependency>

        <!-- Apache Commons Libraries -->
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-lang3</artifactId>
            <version>3.7</version>
        </dependency>
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-collections4</artifactId>
            <version>4.1</version>
        </dependency>

        <!-- Time -->
        <dependency>
            <groupId>joda-time</groupId>
            <artifactId>joda-time</artifactId>
            <version>2.10</version>
        </dependency>

        <!-- Testing -->
        <dependency>
            <groupId>org.spockframework</groupId>
            <artifactId>spock-core</artifactId>
            <version>1.0-groovy-2.4</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.codehaus.groovy</groupId>
            <artifactId>groovy-all</artifactId>
            <version>2.4.5</version>
        </dependency>
        <dependency> <!-- Enable mocking of non-interface types -->
            <groupId>cglib</groupId>
            <artifactId>cglib-nodep</artifactId>
            <version>3.3.0</version>
        </dependency>
        <dependency>
            <groupId>org.objenesis</groupId>
            <artifactId>objenesis</artifactId>
            <version>3.0.1</version>
            <scope>test</scope>
        </dependency>
        <dependency> <!-- ONLY used by JaCoCo to ignore deprecated methods. DO NOT use lombok for all other reasons! -->
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>1.18.10</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>io.rest-assured</groupId>
            <artifactId>rest-assured</artifactId>
            <version>4.1.1</version>
            <scope>test</scope>
        </dependency>

        <!--Jetty-->
        <dependency>
            <groupId>org.eclipse.jetty</groupId>
            <artifactId>jetty-server</artifactId>
            <version>9.4.8.v20171121</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.eclipse.jetty</groupId>
            <artifactId>jetty-webapp</artifactId>
            <version>9.4.8.v20171121</version>
            <scope>provided</scope>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <!-- Publish to artifactory -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-release-plugin</artifactId>
                <version>2.5.3</version>
            </plugin>

            <!-- Use Java 8 -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.7.0</version>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                    <encoding>UTF-8</encoding>
                </configuration>
            </plugin>

            <!-- Java Checkstyle -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-checkstyle-plugin</artifactId>
                <version>3.0.0</version>
                <dependencies>
                    <!-- override default checkstyle version -->
                    <dependency>
                        <groupId>com.puppycrawl.tools</groupId>
                        <artifactId>checkstyle</artifactId>
                        <version>8.10</version>
                    </dependency>
                </dependencies>
                <executions>
                    <execution>
                        <id>validate</id>
                        <phase>validate</phase>
                        <configuration>
                            <linkXRef>false</linkXRef>
                            <encoding>UTF-8</encoding>
                            <failsOnError>true</failsOnError>
                            <consoleOutput>true</consoleOutput>
                            <configLocation>checkstyle.xml</configLocation>
                            <includeTestSourceDirectory>true</includeTestSourceDirectory>
                            <sourceDirectories>${project.build.sourceDirectory}</sourceDirectories>
                        </configuration>
                        <goals>
                            <goal>check</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <!-- Groovy Spock -->
            <plugin>
                <groupId>org.codehaus.gmavenplus</groupId>
                <artifactId>gmavenplus-plugin</artifactId>
                <version>1.6</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>addSources</goal>
                            <goal>addTestSources</goal>
                            <goal>generateStubs</goal>
                            <goal>compile</goal>
                            <goal>generateTestStubs</goal>
                            <goal>compileTests</goal>
                            <goal>removeStubs</goal>
                            <goal>removeTestStubs</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <!-- Groovy checkstyle -->
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>codenarc-maven-plugin</artifactId>
                <version>0.22-1</version>
                <configuration>
                    <sourceDirectory>${project.basedir}</sourceDirectory>
                    <maxPriority1Violations>0</maxPriority1Violations>
                    <maxPriority2Violations>0</maxPriority2Violations>
                    <maxPriority3Violations>0</maxPriority3Violations>
                </configuration>
                <executions>
                    <execution>
                        <phase>prepare-package</phase>
                        <goals>
                            <goal>codenarc</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <!-- Unite Test -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>2.16</version>
                <configuration>
                    <systemPropertyVariables>
                        <java.awt.headless>true</java.awt.headless>
                    </systemPropertyVariables>
                    <includes>
                        <include>%regex[.*Spec.*]</include>
                    </includes>
                    <excludes>
                        <exclude>%regex[.*ITSpec.*]</exclude>
                    </excludes>
                </configuration>
            </plugin>

            <!-- Integration Test -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-failsafe-plugin</artifactId>
                <version>3.0.0-M4</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>integration-test</goal>
                            <goal>verify</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <includes>
                        <include>**/*ITSpec.*</include>
                    </includes>
                </configuration>
            </plugin>

            <!-- Code Coverage report generation -->
            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <version>0.8.4</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>prepare-agent</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>report</id>
                        <phase>prepare-package</phase>
                        <goals>
                            <goal>report</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-site-plugin</artifactId>
                <version>3.7.1</version>
            </plugin>
        </plugins>
    </build>

    <profiles>
        <!-- Enable HTML tags in Javadoc -->
        <profile>
            <id>disable-java8-doclint</id>
            <activation>
                <jdk>[1.8,)</jdk>
            </activation>
            <properties>
                <doclint>none</doclint>
            </properties>
        </profile>

        <profile>
            <id>test</id>
            <properties>
                <env>test</env>
                <gebEnv>test</gebEnv>
                <jacoco.skip>false</jacoco.skip>
                <maven.test.skip>false</maven.test.skip>
                <skip.unit.tests>false</skip.unit.tests>
            </properties>
        </profile>
    </profiles>

    <reporting>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <version>3.1</version>
                <reportSets>
                    <!-- disable Javadoc qtl -->
                    <reportSet>
                        <id>default</id>
                        <reports>
                            <report>javadoc</report>
                        </reports>
                    </reportSet>
                </reportSets>
                <configuration>
                    <encoding>UTF-8</encoding>
                    <charset>UTF-8</charset>
                    <docencoding>UTF-8</docencoding>
                    <additionalparam>-Xdoclint:none</additionalparam>

                    <!-- https://maven.apache.org/plugins/maven-javadoc-plugin/examples/javadoc-resources.html -->
                    <!-- https://stackoverflow.com/a/28849620 -->
                    <javadocDirectory>${basedir}/src/main/javadoc/resources</javadocDirectory>
                    <docfilessubdirs>true</docfilessubdirs>
                </configuration>
            </plugin>

            <!-- unit tests report -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-report-plugin</artifactId>
                <version>2.19.1</version>
            </plugin>

            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <version>0.8.4</version>
            </plugin>
        </plugins>
    </reporting>
</project>
